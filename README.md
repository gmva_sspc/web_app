Le patrimoine de Golfe du Morbihan Vannes Agglomération (GMVA) a
évolué ces dernières années à la fois en quantité mais aussi en
technicité. Le suivi et l’amélioration des performances énergétiques des
installations communautaires doit permettre de réduire les
consommations énergétiques et ainsi atteindre les objectifs collectifs
du Plan Climat Air Energie territorial, notamment de diminuer de 30%
les consommations du territoire à horizon 2030 par rapport à 2012.

L’objectif du projet proposé est d’analyser les données de suivi des
installations électriques, chauffage, ventilation, climatisation en
vue de l’amélioration de l’efficacité énergétique du patrimoine et le
suivi des installations de production d’énergie renouvelable de la
collectivité et ainsi proposer les modules de régulation et des
systèmes d’alerte pouvant être fonction, par exemple de données de
prévisions météorologiques, etc.

Dans ce projet, il s'agira de concevoir une application Web permettant 
* d'ajouter des sources de de données énergétiques (installations
  électriques, chauffage, ventilation, climatisation)
* de définir les traitements à appliquer aux données collectées pour
  qu'elles répondent au format défini dans le projet
  <https://gitlab.com/gmva_sspc/datasrc>
* d'intéger les prévisions météorologiques obtenues par le projet <https://gitlab.com/gmva_sspc/meteo_prev>
* de lever des alertes pour proposer des ajustements en fonction de
  données météorologiques et des informations provenant des sources de
  de données énergétiques.

Cette application comportera 2 parties: une partie permettant la
consultation des données, et une partie permettant de paramétrer
l'application (ajout, suppression, modification des sources
énergétiques, ajout, suppression, modification des fournisseurs de
données météorologiques, définition des seuils et mécanismes
d'alertes). Cette application devra reprendre la charte graphique de
GMVA.
